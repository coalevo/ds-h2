/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.h2.service;

/**
 * Defines a tag interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see net.coalevo.foundation.util.BundleConfiguration
 */
public interface H2DataSourceProviderConfiguration {

  /**
   * Defines the key for the encryption flag configuration.
   */
  public static final String ENCRYPTION_KEY = "encryption.enabled";

  /**
   * Defines the key for the encryption password configuration.
   */
  public static final String ENCRYPTION_PASSWORD_KEY = "encryption.password";

  /**
   * Defines the key for the encryption algorithm configuration.
   */
  public static final String ENCRYPTION_ALGORITHM_KEY = "encryption.algorithm";

  /**
   * Defines the key for the compatibility mode configuration.
   */
  public static final String COMPATIBILITY_MODE_KEY = "compatibility.mode";

  /**
   * Defines the key for the encryption flag configuration.
   */
  public static final String CASEINSENSITIVE_KEY = "caseinsensitive.enabled";


  /**
   * Defines the key for special settings configuration.
   */
  public static final String SETTINGS_KEY = "specialsettings";

}//interface H2DataSourceProviderConfiguration
