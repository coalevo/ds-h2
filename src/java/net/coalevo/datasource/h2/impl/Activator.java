/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.h2.impl;

import net.coalevo.datasource.h2.service.H2DataSourceProviderConfiguration;
import net.coalevo.datasource.model.DataSourceProvider;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.logging.model.LogProxy;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;

/**
 * This class implements the bundle's activator.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static Marker c_LogMarker;
  private static LogProxy c_Log;

  private static ServiceMediator c_Services;
  private static Messages c_BundleMessages;
  private static BundleConfiguration c_BundleConfiguration;
  private static H2DataSourceProvider c_DataSourceProvider;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext)
      throws Exception {
    if (m_StartThread != null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            //1. Log
            c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
            c_Log = new LogProxy();
            c_Log.activate(bundleContext);

            //2. Services
            c_Services = new ServiceMediator();
            c_Services.activate(bundleContext);


            //3. Bundle Messages
            c_BundleMessages = c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                .getBundleMessages(bundleContext.getBundle());
            if (c_BundleMessages == null) {
              log().error(c_LogMarker, "Bundle message resources could not be resolved.");
              return;
            }

            //4. Bundle Configuration
            c_BundleConfiguration = new BundleConfiguration(H2DataSourceProviderConfiguration.class.getName());
            c_BundleConfiguration.activate(bundleContext);
            c_Services.setConfigurationMediator(c_BundleConfiguration.getConfigurationMediator());

            //5. Prepare embedded store directory
            File f = bundleContext.getDataFile("");
            File dir = new File(f, "embedded_store");
            if(!dir.exists() && !dir.mkdir()) {
              log().info(c_LogMarker, c_BundleMessages.get("Internal embedded store could not be created."));
            }

            //6. Register Provider
            c_DataSourceProvider = new H2DataSourceProvider(dir);
            c_DataSourceProvider.activate();
            bundleContext.registerService(
                DataSourceProvider.class.getName(),
                c_DataSourceProvider,
                null
            );
            log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.datasource", "ds", "H2DataSourceProvider"));

          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {
    if (m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }

    if (c_DataSourceProvider != null) {
      c_DataSourceProvider.deactivate();
      c_DataSourceProvider = null;
    }
    if (c_BundleConfiguration != null) {
      c_BundleConfiguration.deactivate();
      c_BundleConfiguration = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    if (c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }
    c_BundleMessages = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator
