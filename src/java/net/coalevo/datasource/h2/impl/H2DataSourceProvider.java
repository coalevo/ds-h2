/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.h2.impl;

import net.coalevo.datasource.h2.service.H2DataSourceProviderConfiguration;
import net.coalevo.datasource.model.DataSourceProvider;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Implements a {@link DataSourceProvider} for a H2
 * Database.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class H2DataSourceProvider
    implements DataSourceProvider, ConfigurationUpdateHandler {

  private File m_StoreDir;
  private DataSource m_DataSource;
  private Marker m_LogMarker = MarkerFactory.getMarker(getClass().getName());

  public H2DataSourceProvider(File storedir) {
    m_StoreDir = storedir;
  }//constructor

  public String getIdentifier() {
    return "h2";
  }//getIdentifier

  public DataSource getDataSource() {
    return m_DataSource;
  }//getDataSource

  public void activate() {
    ConfigurationMediator cm = Activator.getServices().getConfigurationMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();
    m_DataSource = new JdbcDataSource();
    configure(mtd);
  }//activate

  public void deactivate() {
    m_DataSource = null;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {
    configure(mtd);
  }//update

  private void configure(MetaTypeDictionary mtd) {
    String url = "jdbc:h2:";
    boolean encrypt = false;
    String encpass = "";
    String algorithm = "";
    String mode = "NONE";
    boolean caseinsensitive = false;
    String settings = "";

    try {
      encrypt = mtd.getBoolean(H2DataSourceProviderConfiguration.ENCRYPTION_KEY).booleanValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get(
              "H2DataSourceProvider.configexception",
              "attribute",
              H2DataSourceProviderConfiguration.ENCRYPTION_KEY),
          ex
      );
    }
    if (encrypt) {
      try {
        encpass = mtd.getString(H2DataSourceProviderConfiguration.ENCRYPTION_PASSWORD_KEY);
      } catch (MetaTypeDictionaryException ex) {
        Activator.log().error(m_LogMarker,
            Activator.getBundleMessages().get(
                "H2DataSourceProvider.configexception",
                "attribute",
                H2DataSourceProviderConfiguration.ENCRYPTION_PASSWORD_KEY),
            ex
        );
        encrypt = false;
      }
      try {
        algorithm = mtd.getString(H2DataSourceProviderConfiguration.ENCRYPTION_ALGORITHM_KEY);
      } catch (MetaTypeDictionaryException ex) {
        Activator.log().error(m_LogMarker,
            Activator.getBundleMessages().get(
                "H2DataSourceProvider.configexception",
                "attribute",
                H2DataSourceProviderConfiguration.ENCRYPTION_ALGORITHM_KEY),
            ex
        );
      }
    }//encrypt
    try {
      mode = mtd.getString(H2DataSourceProviderConfiguration.COMPATIBILITY_MODE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get(
              "H2DataSourceProvider.configexception",
              "attribute",
              H2DataSourceProviderConfiguration.COMPATIBILITY_MODE_KEY),
          ex
      );
    }
    try {
      caseinsensitive = mtd.getBoolean(H2DataSourceProviderConfiguration.CASEINSENSITIVE_KEY).booleanValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get(
              "H2DataSourceProvider.configexception",
              "attribute",
              H2DataSourceProviderConfiguration.CASEINSENSITIVE_KEY),
          ex
      );
    }
    try {
      settings = mtd.getString(H2DataSourceProviderConfiguration.SETTINGS_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get(
              "H2DataSourceProvider.configexception",
              "attribute",
              H2DataSourceProviderConfiguration.SETTINGS_KEY),
          ex
      );
    }
    StringBuilder sb = new StringBuilder(url);
    sb.append(m_StoreDir.getAbsolutePath());
    sb.append(File.separator);
    sb.append("data");
    if(encrypt) {
      sb.append(";CIPHER=").append(algorithm);
    }
    if(!"NONE".equals(mode)) {
      sb.append(";MODE=").append(mode);
    }
    if(caseinsensitive) {
      sb.append(";IGNORECASE=TRUE");
    }
    if(settings.length() > 0) {
      sb.append(";").append(settings);
    }

    synchronized (m_DataSource) {
      JdbcDataSource mds = (JdbcDataSource) m_DataSource;
      if(encrypt) {
        mds.setPassword(encpass);
      }
      mds.setURL(sb.toString());
    }
    checkConnection();
  }//configure

  private void checkConnection() {
    try {
      Connection c = m_DataSource.getConnection();
      c.close();
      Activator.log().info("checkConnection():true");
    } catch (SQLException ex) {
      Activator.log().error(m_LogMarker, "checkConnection()", ex);
    }
  }//checkConnection

}//class H2DataSourceProvider
